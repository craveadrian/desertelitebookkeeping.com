<div id="services">
	<div class="row">
		<dl class="col-4 fl">
			<dt> <img src="public/images/content/svc1.jpg" alt="Service Images"> </dt>
			<dd> <p>INCOME TAX PREPARATION <span>(ALL STATES) </span></p>
					<a href="#" class="btn2">MORE</a>
			</dd>
		</dl>
		<dl class="col-4 fl">
			<dt> <img src="public/images/content/svc2.jpg" alt="Service Images">
			</dt>
			<dd> <p>BOOKKEEPING & ACCOUNTING</p>
				<a href="#" class="btn2">MORE</a>
			</dd>
		</dl>
		<dl class="col-4 fl">
			<dt> <img src="public/images/content/svc3.jpg" alt="Service Images">
			</dt>
			<dd> <p>DOCUMENT PROCESSING</p>
				<a href="#" class="btn2">MORE</a>
			</dd>
		</dl>
		<dl class="col-4 fl">
			<dt> <img src="public/images/content/svc4.jpg" alt="Service Images">
			</dt>
			<dd> <p>BUSINESS CONSULTING</p>
				<a href="#" class="btn2">MORE</a>
			</dd>
		</dl>
		<div class="clearfix"></div>
	</div>
</div>
<div id="about">
	<div class="row">
			<a href="<?php echo URL ?>"> <img src="public/images/common/mainLogo.png" alt="<?php $this->info("company_name"); ?> Main Logo"> </a>
			<p class="point">&#x25CF;</p>
			<p>For tax preparation in Indio CA, you can count on <?php $this->info("company_name"); ?>. We assists taxpayers and small businesses with taxes in Indio CA and the surrounding communities. Whether you are an individual or a local business in or around Indio CA, Amanda G. Hodges  has years of valuable experience as an IRS registered tax preparer. Contact Amanda G. Hodges, tax filing specialist in Indio CA, for help with your taxes.</p>
			<a href="about#content" class="btn2"> LEARN MORE</a>
	</div>
</div>
<div id="chooseus">
	<div class="row">
		<h2>Looking to find the best rated tax preparer in Indio CA?</h2>
		<div class="wcTop">
			<div class="topLeft col-6 fl">
				<img src="public/images/content/wc1.jpg" alt="pen">
			</div>
			<div class="topRight col-6 fl">
				<div class="section">
					<p><strong><?php $this->info("company_name"); ?></strong><br> is a local tax preparer  located in Indio CA. Amanda G. Hodges and other tax preparers located in Indio CA will help you with.</p>
					<ul>
						<li><p><img src="public/images/content/check.png" alt="check"> <span>TAX PREPARATION</span></p></li>
						<li><p><img src="public/images/content/check.png" alt="check"> <span>TAX PLANNING</span></p></li>
						<li><p><img src="public/images/content/check.png" alt="check"> <span>BOOKKEEPING</span></p></li>
						<li><p><img src="public/images/content/check.png" alt="check"> <span>ESTATE & TRUST TAXES</span></p></li>
					</ul>
					<p> <a href="services#content"> & SO MUCH MORE </a></p>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
		<div class="wcBot">
			<div class="botLeft col-6 fl">
				<div class="section">
					<h3>Why Choose Us?</h3>
					<ul>
						<li><p><img src="public/images/content/check.png" alt="check"> <span>Flexible Hours</span></p></li>
						<li><p><img src="public/images/content/check.png" alt="check"> <span>Attention To Detail</span></p></li>
						<li><p><img src="public/images/content/check.png" alt="check"> <span>Highly Experienced</span></p></li>
						<li><p><img src="public/images/content/check.png" alt="check"> <span>Evening Appointments</span></p></li>
						<li><p><img src="public/images/content/check.png" alt="check"> <span>Weekend Appointments</span></p></li>
						<li><p><img src="public/images/content/check.png" alt="check"> <span>Individual & Business Tax Services Available</span></p></li>
						<li><p><img src="public/images/content/check.png" alt="check"> <span>QB Training Available</span></p></li>
					</ul>
				</div>
			</div>
			<div class="botRight col-6 fl">
				<img src="public/images/content/wc2.jpg" alt="meeting">
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
</div>
<div id="contact">
	<div class="hdContact">
		<div class="row">
			<h1>Contact Us</h1>
			<p>Please fill out the form below and we will get back to you as soon as we can with a reply. Thank you.</p>
		</div>
	</div>
	<div class="contactForm">
		<div class="row">
			<form action="sendContactForm" method="post"  class="sends-email ctc-form" >
					<input type="text" name="name" placeholder="Name:">
					<input type="text" name="phone" placeholder="Phone:">
					<input type="text" name="email" placeholder="Email:">
					<textarea name="message" cols="30" rows="10" placeholder="Message / Questions:"></textarea>
				<div class="g-recaptcha"></div>
				<label>
					<input type="checkbox" name="consent" class="consentBox">I hereby consent to having this website store my submitted information so that they can respond to my inquiry.
				</label><br>
				<?php if( $this->siteInfo['policy_link'] ): ?>
				<label>
					<input type="checkbox" name="termsConditions" class="termsBox"/> I hereby confirm that I have read and understood this website's <a href="<?php $this->info("policy_link"); ?>" target="_blank">Privacy Policy.</a>
				</label>
				<?php endif ?>
				<button type="submit" class="ctcBtn btn3" disabled>SUBMIT FORM</button>
			</form>
		</div>
	</div>
</div>
